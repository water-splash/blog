---
title: hexo 配置 gulp
author: Roc
abbrlink: b37c0e7e
categories:
  - 工具 
  - hexo
tags:
  - hexo
  - gulp
date: 2021-07-17 12:48:30
---

## 安装 npm 包

``` shell
npm install gulp gulp-clean-css gulp-htmlmin gulp-htmlclean gulp-babel gulp-uglify gulp-imagemin gulp-babel babel-preset-env gifsicle
```

## 在博客根目录下添加 `gulpfile.js` 文件

``` js
let gulp = require('gulp')
let cleanCSS = require('gulp-clean-css')
let htmlmin = require('gulp-htmlmin')
let htmlclean = require('gulp-htmlclean')
let babel = require('gulp-babel') /* 转换为es2015 */
let uglify = require('gulp-uglify')
let imagemin = require('gulp-imagemin')

// 设置根目录
const root = './public'

// 匹配模式， **/*代表匹配所有目录下的所有文件
const pattern = '**/*'

// 压缩html
gulp.task('minify-html', function() {
  return gulp
    // 匹配所有 .html结尾的文件
    .src(`${root}/${pattern}.html`)
    .pipe(htmlclean())
    .pipe(
      htmlmin({
        removeComments: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      })
    )
    .pipe(gulp.dest('./public'))
})

// 压缩css
gulp.task('minify-css', function() {
  return gulp
    // 匹配所有 .css结尾的文件
    .src(`${root}/${pattern}.css`)
    .pipe(
      cleanCSS({
        compatibility: 'ie8'
      })
    )
    .pipe(gulp.dest('./public'))
})

// 压缩js
gulp.task('minify-js', function() {
  return gulp
    // 匹配所有 .js结尾的文件
    .src(`${root}/${pattern}.js`)
    .pipe(
      babel({
        presets: ['env']
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest('./public'))
})

// 压缩图片
gulp.task('minify-images', function() {
  return gulp
    // 匹配public/images目录下的所有文件
    .src(`${root}/images/${pattern}`)
    .pipe(
      imagemin(
        [
          imagemin.gifsicle({ optimizationLevel: 3 }),
          imagemin.mozjpeg({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 7 }),
          imagemin.svgo()
        ],
        { verbose: true }
      )
    )
    .pipe(gulp.dest('./public/images'))
})

gulp.task('default', gulp.series('minify-html', 'minify-css', 'minify-js', 'minify-images'))
```

## 修改 `gitlab-ci.yml` 文件

``` yml
before_script:
  - npm install -g gulp
  
pages:
  script:
    - gulp
```

参考：

https://www.fadai.cc/posts/97bf547e/
https://www.voidking.com/dev-hexo-gulp/
https://alderaan.xyz/2020/05/07/hexo-compress/?utm_source=pocket_mylist