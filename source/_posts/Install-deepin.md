---
title: Install deepin
author: Kawhicurry
categories:
  - Backend
  - OS
abbrlink: 9f6e02da
---

## 2021/5/25

This is my `third` times installing deepin.

I will record problems I met while installing it.

### Step 1: Install
Thanks to Roc,I ues [Ventoy](https://www.ventoy.net/cn/index.html) to make a boot device.
All you need to do are follow the lead by ventoy and move iso files to your device.
Then reboot and enter bios interface and choose your boot device.

`But before that` I recommend you to divide thr partition on your disk.Win10 supports this operation already,or you can ues PE tools.
My partition-pattern are like below:
```
/:128G  <-This is the root directory
/swap:8G    <-This is the swap directory,mainly for suspend or hibernate.
```
Of course you can adjust your personal situation or you may feel puzzle to do that.Thus you can install with option`Full disk install`.

`Notice`: In the second time I install deepin,I divide my partition like this:
```
/:128G
/usr:16G    <-For user files
/swap:8G
```
It's clearly that my user partition are too small thus when I download videos,deepin warned me of `intense space`.
The system files are put on `/` or `/boot`,so divide them as isolated part is a good idea.But for a lazy man like me,I simply install them as a whole.
The advantage of not divide them is clear:you won't manage too mush partition.But if your system breaks(Linux desk distributions frequently occur),and you have to reinstall your OS,your personal files will lose absolutely.But for me,It's doesn't matter,for I put most of my files on moveable device or sync by `Git`.

### Step 2: Environment
My target to install deepin is to create a environment for study without games and exploring computer simple.
This is my advice on using deepin:

1. default settings:
   I advice you log on a deepin account so you can sync your settings and join the community of deepin.Then choose your personal settings according to `system settings`
2. install software:
   It's great to install by click `install` in `app store` provided by deepin.But I advice you not doing that.Installed from commend line will help you get newest version and may accepted by most users in linux(maybe?).But some applications like `wechat` or `WPS` are preferred to install in `app store`.//TODO:write about environment.
3. double system:
   You may install deepin along with Windows like me. So I will provide a little advice about how to manage your system:
   1. Use `refind` to boot your windows rather than `grub`(grub of deepin may not boot from Windows successfully).//TODO:write about refind.
   2. Frequently update
   update everything!Or you may suffer some problems and had to reinstall your OS like me.Linux may break easily and it's user's chance to manage it.

I got my idea interrupt here.I will catch my mind to set down more about these later(maybe?).
