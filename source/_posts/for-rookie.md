---
title: For Rookie
categories: Trivial
tags: Rookie
author: Roc
abbrlink: '26383158'
date: 2021-05-26 08:03:16
---

欢迎来到菜鸟页面，这是专门为某位菜鸟准备的一个散乱的知识集。

# Update Log

## 2020-5

### 5-26

1. `add` git 分支的最基本的操作

------

# 2020-5

## 5-26 git 分支的最基本的操作

创建分支命令：

```bash
git branch <branchname>
```

切换分支命令：

```bash
git checkout <branchname>
```

列出本地分支：

```bash
git branch
```

列出所有分支：

```bash
git branch -a
```

合并分支：

```bash
git merge
```

这种情况下在 `main` 分支执行 `git merge topic` 

```
	      A---B---C topic
         /
    D---E           main
```

会得到以下结果

```
	      A---B---C    topic
         /
    D---E---A---B---C   main
```

当然，这种情况终归是少数。如果是下面这种情况

```
	      A---B---C topic
	     /
    D---E---F---G main
```

执行同样的命令后我们应该会得到这个

```
	      A---B---C topic
	     /         \
    D---E---F---G---H main
```

这种情况就会发生冲突，只有等我们处理完冲突后我们才能够完全合并。

例如：

`topic` 分支里有个 `README.txt` 

```
this is commit A
no, it's commit C
```

`main` 分支里有个 `README.txt` 

```
this is commit A
no, it's commit H
```

尝试把 `topic` 合并到 `main` 里

```bash
> git merge topic
Auto-merging README.txt
CONFLICT (content): Merge conflict in README.txt
Automatic merge failed; fix conflicts and then commit the result.
```

很好，说是先修复冲突，现在的 `README.txt` 长这样

```bash
> cat README.txt
this is commit A
<<<<<<< HEAD
no, it's commit H
=======
no, it's commit C
>>>>>>> topic
```

还有现在的状态

```bash
> git status
On branch main
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
        both modified:   README.txt

```

现在就很清晰了，把文件改了然后再提交一次。

好了，问题来了，一个小冲突好解决，如果遇到很多需要处理的冲突咋办呢。有没有更方便的办法或者工具呢。还有，这样操作下来可能会丢失很多的提交记录，如果我想保留提交记录咋办呢。

**Reference**:

1. [Git - git-merge Documentation (git-scm.com)](https://git-scm.com/docs/git-merge)

